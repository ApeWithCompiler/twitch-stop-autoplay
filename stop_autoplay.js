// Searches for the player element
// If found it is muted and paused
function silenceVideoPlayer(){
    let frontpage_player = document.querySelectorAll('[data-a-player-type="frontpage"]');
    if (frontpage_player.length == 0) {
      return;
    }
    let video = frontpage_player[0].getElementsByTagName("video");
    if (video.length == 0) {
      return;
    }
    video[0].muted = true;
    video[0].pause();
}

// Sets intervall to make sure function is executed
// whenever player has finished loading
window.setInterval(function(){
  silenceVideoPlayer();
}, 100);