# ![logo](images/icon48.png) Twitch Stop Autoplay

A chrome extension that mutes and pauses the twitch player on the frontpage.

## Getting started

### Install from .crx package
To install the addon from the prepacked .crx file follow these steps:
* Settings - icon > Tools > Extensions
* Enable Developer Mode ( toggle button in top-right corner )
* Drag and drop the '.crx' extension file onto the Extensions page from step 1
( crx file should likely be in your Downloads directory )
* Install

### Install from source
To install the addon from the unpacked source files follow these steps:
* Settings - icon > Tools > Extensions
* Enable Developer Mode ( toggle button in top-right corner )
* Click on Load Unpacked and select the unpacked folder. Note: You need to select the folder in which the manifest file exists
* The extension will be installed now