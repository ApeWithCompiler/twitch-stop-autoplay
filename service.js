chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    files: ['stop_autoplay.js']
  });
});